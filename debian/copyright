Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: go-wildcard
Upstream-Contact: https://github.com/IGLOU-EU/go-wildcard/issues
Source: https://github.com/IGLOU-EU/go-wildcard

Files: *
Copyright: 2015-2016 MinIO, Inc.
           2021-2023 Adrien Kara <adrien@iglou.eu>
Comment:
 This library remains under Apache License Version 2.0, but MinIO project is
 migrated to GNU Affero General Public License 3.0 or later from
 https://github.com/minio/minio/commit/069432566fcfac1f1053677cc925ddafd750730a
License: Apache-2.0

Files: debian/*
Copyright: 2022-2023 Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".
